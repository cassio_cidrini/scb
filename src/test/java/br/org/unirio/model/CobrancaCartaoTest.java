package br.org.unirio.model;
import org.junit.Test;
import static org.junit.Assert.*;

public class CobrancaCartaoTest {

    @Test
    public void testValorPositivo() {
        double valor = 80.0;
        CobrancaCartao cartao = new CobrancaCartao(123456789, "PH", "11/07/2021", 007,
               149014888, "Rua Alfinete", 0.0);
        cartao.setValorCobranca(valor);
        assertEquals(cartao.getValorCobranca(), 80.0,0.00);

   }




    @Test(expected=IllegalArgumentException.class)
    public void testaValorNegativo(){
        CobrancaCartao.verificaCobranca(-19.90);
    }




}
