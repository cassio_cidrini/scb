package br.org.unirio.repository;

import br.org.unirio.model.CobrancaCartao;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

public class CartaoRepositoryTest {
    private static CartaoRepositoryTest instance;
    private ArrayList<CobrancaCartao> cobrancas;

    public static CartaoRepositoryTest getInstanceTest(){
        if(instance == null)
            instance = new CartaoRepositoryTest();
        return instance;
    }


    @Test
    public void adicionaCobrancaTest() {
        cobrancas = new ArrayList<>();
        cobrancas.add(new CobrancaCartao(123456789, "PH", "11/07/2021", 007,
                149014888, "Rua Alfinete", 80.0));

        assertEquals(cobrancas.get(0).getNumero(),123456789);
        assertEquals(cobrancas.get(0).getProprietario(),"PH");
        assertEquals(cobrancas.get(0).getValidade(),"11/07/2021");
        assertEquals(cobrancas.get(0).getCodigo(),007);
        assertEquals(cobrancas.get(0).getCpf(),149014888);
        assertEquals(cobrancas.get(0).getEndereco(),"Rua Alfinete");
        assertEquals(cobrancas.get(0).getValorCobranca(),80.0, 0.00);
    }
}
