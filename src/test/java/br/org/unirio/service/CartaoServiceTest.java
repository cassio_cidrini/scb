package br.org.unirio.service;

import br.org.unirio.model.CobrancaCartao;
import br.org.unirio.repository.CartaoRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class CartaoServiceTest {

    @Mock
    private CartaoRepository cartaoRepo;

    @InjectMocks
    private CartaoService cs = new CartaoService();
    private List<CobrancaCartao> mockCobrancaCartao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockCobrancaCartao = new ArrayList<>();
        mockCobrancaCartao.add(new CobrancaCartao(123456789, "PH", "11/07/2021", 007,
                149014888, "Rua Alfinete", 80.0));
        mockCobrancaCartao.add(new CobrancaCartao(2345678, "Jow", "31/01/2022", 001,
                339274833, "Rua Pascal", 120.0));
        mockCobrancaCartao.add(new CobrancaCartao(836248236, "David", "28/02/2023", 002,
                149333888, "Rua Nanadaime", 11.0));
        mockCobrancaCartao.add(new CobrancaCartao(36423034, "Correa", "15/09/2021", 003,
                333014888, "Av. Mauricio Isla", 1.99));
        mockCobrancaCartao.add(new CobrancaCartao(216823934, "Law", "11/12/2021", 004,
                149014333, "Rua Gonzaguinha", 400000.99));
    }

    @Test
    public void cobraCartaoTestExiste(){
        CobrancaCartao c =  new CobrancaCartao(mockCobrancaCartao.get(0).getNumero(),
                mockCobrancaCartao.get(0).getProprietario(),
                "11/07/2021",
                007,
                149014888,
                mockCobrancaCartao.get(0).getEndereco(),
                80.0);

        doReturn(null).when(cartaoRepo).listaCobrancas();
        doNothing().when(cartaoRepo).adicionaCobranca(c);

        try {
            cs.cobraCartao(mockCobrancaCartao.get(0).getNumero(),
                    mockCobrancaCartao.get(0).getProprietario(),
                    mockCobrancaCartao.get(0).getValidade(),
                    007,
                    149014888,
                    mockCobrancaCartao.get(0).getEndereco(),
                    80.0);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void cobraCartaoTestNaoExiste(){
        CobrancaCartao c =  new CobrancaCartao(mockCobrancaCartao.get(1).getNumero(),
                mockCobrancaCartao.get(1).getProprietario(),
                "09/0/1999",
                001,
                339274833,
                mockCobrancaCartao.get(1).getEndereco(),
                99.99);

        doReturn(null).when(cartaoRepo).listaCobrancas();
        doNothing().when(cartaoRepo).adicionaCobranca(c);

        try {
            cs.cobraCartao(mockCobrancaCartao.get(1).getNumero(),
                    mockCobrancaCartao.get(1).getProprietario(),
                    "09/0/1999",
                    001,
                    339274833,
                    mockCobrancaCartao.get(1).getEndereco(),
                    99.99);
        } catch (Exception e) {
            Assert.fail();
        }
    }


}
