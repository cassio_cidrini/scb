package br.org.unirio.service;

import org.junit.Test;

public class EmailServiceTest {

    @Test(expected=IllegalArgumentException.class)
    public void enviaEmailTest(){
        EmailService envEmail = new EmailService();
        envEmail.enviaEmail("", "Email vazio");
    }
}
