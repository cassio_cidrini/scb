package br.org.unirio.controller;

import br.org.unirio.service.EmailService;
import io.javalin.http.Context;

public class EmailController {
    private EmailController(){}


    public static void enviaEmail(Context context) {
        EmailService emailService = new EmailService();

        try {
            String email = context.queryParam("email");
            String corpoEmail = context.queryParam("mensagem");

            emailService.enviaEmail(email, corpoEmail);

            context.status(200);
        } catch (Exception e){
            context.status(400);
        }
    }
}
