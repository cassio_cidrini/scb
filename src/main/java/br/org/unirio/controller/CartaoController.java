package br.org.unirio.controller;

import br.org.unirio.service.CartaoService;
import io.javalin.http.Context;

import java.util.Objects;

public class CartaoController {

    public static void cobraCartao(Context context) {
        try{
            CartaoService cartaoService = new CartaoService();

            long numero = Long.parseLong(Objects.requireNonNull(context.queryParam("numero")));
            String proprietario = Objects.requireNonNull(context.queryParam("proprietario"));
            String validade = Objects.requireNonNull(context.queryParam("validade"));
            int codigo = Integer.parseInt(Objects.requireNonNull(context.queryParam("codigo")));
            long cpf = Long.parseLong(Objects.requireNonNull(context.queryParam("cpfProprietario")));
            String endereco = Objects.requireNonNull(context.queryParam("enderecoCobranca"));
            double valorCobranca = Double.parseDouble(Objects.requireNonNull(context.queryParam("valorCobranca")));

            cartaoService.cobraCartao(numero, proprietario, validade, codigo, cpf, endereco, valorCobranca);
        } catch (Exception e){
            context.status(400);
        }
    }

    public static void getCobrancasCartao(Context context) {
        CartaoService cartaoService = new CartaoService();

        context.json(cartaoService.listaCobrancas());
    }
}
