package br.org.unirio.repository;

import br.org.unirio.model.CobrancaCartao;

import java.util.ArrayList;

public class CartaoRepository {
    private static CartaoRepository instance;
    private ArrayList<CobrancaCartao> cobrancas;

    public static CartaoRepository getInstance(){
        if(instance == null)
            instance = new CartaoRepository();
        return instance;
    }

    private CartaoRepository(){
        cobrancas = new ArrayList<>();
    }

    public void adicionaCobranca(CobrancaCartao novaCobranca) {
        cobrancas.add(novaCobranca);
    }

    public ArrayList<CobrancaCartao> listaCobrancas() {
        return cobrancas;
    }
}
