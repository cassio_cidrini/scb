package br.org.unirio.service;

import br.org.unirio.util.GmailAPI;
import org.apache.commons.lang3.StringUtils;

public class EmailService {

    public void enviaEmail(String email, String mensagem) throws IllegalArgumentException {
        if(StringUtils.isEmpty(email))
            throw new IllegalArgumentException();

        GmailAPI.sendMail(mensagem, email, "Email do Bicicletario - PM");
    }
}
