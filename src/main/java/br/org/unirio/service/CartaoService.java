package br.org.unirio.service;

import br.org.unirio.model.CobrancaCartao;
import br.org.unirio.repository.CartaoRepository;

import java.util.ArrayList;

public class CartaoService {

    public void cobraCartao(long numero, String proprietario, String validade, int codigo, long cpfProprietario,
                            String enderecoCobranca, double valorCobranca){
        CobrancaCartao novaCobranca = new CobrancaCartao(numero, proprietario, validade, codigo, cpfProprietario,
                enderecoCobranca, valorCobranca);

        CartaoRepository.getInstance().adicionaCobranca(novaCobranca);
    }

    public ArrayList<CobrancaCartao> listaCobrancas() {
        return CartaoRepository.getInstance().listaCobrancas();
    }
}
