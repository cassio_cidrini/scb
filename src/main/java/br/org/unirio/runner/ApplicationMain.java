package br.org.unirio.runner;

import br.org.unirio.controller.*;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

public class ApplicationMain {
    public static void main(String[] args) {
        Javalin javalin = Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
        }).start(8080);
        javalin.routes(() -> {
            path("enviaEmail", () -> {
                post(EmailController::enviaEmail);
            });
            path("cobraCartao", () -> {
                post(CartaoController::cobraCartao);
                get(CartaoController::getCobrancasCartao);
            });
        });
    }

    private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").title("User API").description("Demo API with 5 operations");
        OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("io.javalin.example.java")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")); // endpoint for redoc
        return new OpenApiPlugin(options);
    }
}
