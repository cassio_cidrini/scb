package br.org.unirio.model;

public class CobrancaCartao {
    private long numero;
    private String proprietario;
    private String validade;
    private int codigo;
    private long cpf;
    private String endereco;


    private double valorCobranca;

    public CobrancaCartao(long numero, String proprietario, String validade, int codigo, long cpfProprietario,
                          String enderecoCobranca, double valorCobranca){
        this.numero = numero;
        this.proprietario = proprietario;
        this.validade = validade;
        this.codigo = codigo;
        this.cpf = cpfProprietario;
        this.endereco = enderecoCobranca;
        this.valorCobranca = valorCobranca;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public String getValidade() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public double getValorCobranca() {
        return valorCobranca;
    }

    public void setValorCobranca(double valorCobranca) throws IllegalArgumentException {
        verificaCobranca(valorCobranca);
        this.valorCobranca = valorCobranca;
    }

    static void verificaCobranca(Double valor) {
        if (valor <= 0)
            throw new IllegalArgumentException("Valor negativo");
    }
}
